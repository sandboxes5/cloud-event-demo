package org.example.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class Address implements Serializable {
    private static final long serialVersionUID = 1L;
    private String city;
    private String street;
}
