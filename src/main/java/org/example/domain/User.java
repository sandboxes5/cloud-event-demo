package org.example.domain;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Data
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    private long id;
    private String name;
    private LocalDate birthDate;
    private Address address;
}
