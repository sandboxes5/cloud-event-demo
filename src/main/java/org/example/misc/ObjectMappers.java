package org.example.misc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.cloudevents.jackson.JsonFormat;
import lombok.Getter;

@Getter
public class ObjectMappers {
    public static final ObjectMappers OM_WITHOUT_CLOUD_EVENT_MODULE;
    public static final ObjectMappers OM_WITH_CLOUD_EVENT_MODULE;

    static {
        final Jdk8Module jdk8Module = new Jdk8Module();
        final JavaTimeModule javaTimeModule = new JavaTimeModule();
        {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.registerModule(jdk8Module);
            objectMapper.registerModule(javaTimeModule);
            OM_WITHOUT_CLOUD_EVENT_MODULE = new ObjectMappers("Without CloudEvent", objectMapper);
        }
        {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.registerModule(jdk8Module);
            objectMapper.registerModule(javaTimeModule);
            objectMapper.registerModule(JsonFormat.getCloudEventJacksonModule());
            OM_WITH_CLOUD_EVENT_MODULE = new ObjectMappers("With CloudEvent", objectMapper);
        }
    }

    private final String desc;
    private final ObjectMapper objectMapper;

    private ObjectMappers(String desc, ObjectMapper objectMapper) {
        this.desc = desc;
        this.objectMapper = objectMapper;
    }
}
