package org.example.misc;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SerializationResult {
    private final String payloadSerializer;
    private final String cloudEventSerializer;
    private final Result result;
}
