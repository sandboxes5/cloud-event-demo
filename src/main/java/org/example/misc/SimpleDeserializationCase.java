package org.example.misc;

import io.cloudevents.CloudEvent;
import lombok.AllArgsConstructor;
import org.example.converter.deserialize.CloudEventDeserializer;
import org.example.converter.deserialize.PayloadDeserializer;

@AllArgsConstructor
public class SimpleDeserializationCase {
    private final PayloadDeserializer payloadDeserializer;
    private final CloudEventDeserializer cloudEventDeserializer;

    public Result execute(final String json) {
        Result result = cloudEventDeserializer.deserialize(json);

        if (result.isSuccess()) {
            final CloudEvent cloudEvent = (CloudEvent) result.getResult();
            result = payloadDeserializer.deserialize(cloudEvent.getData());
        }
        return result;
    }
}
