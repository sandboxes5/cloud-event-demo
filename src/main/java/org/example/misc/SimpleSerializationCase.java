package org.example.misc;

import io.cloudevents.CloudEventData;
import lombok.AllArgsConstructor;
import org.example.converter.serialize.CloudEventSerializer;
import org.example.converter.serialize.PayloadSerializer;
import org.example.domain.User;

@AllArgsConstructor
public class SimpleSerializationCase {
    private final CloudEventSerializer eventSerializer;
    private final PayloadSerializer payloadSerializer;

    public Result execute(final User user) {
        final Result<CloudEventData> cloudEventDataResult = payloadSerializer.serialize(user);
        if (cloudEventDataResult.isSuccess()) {
            return eventSerializer.serialize(user, cloudEventDataResult.getResult());
        }

        return cloudEventDataResult;
    }
}
