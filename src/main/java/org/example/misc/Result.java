package org.example.misc;

import lombok.Getter;

@Getter
public class Result<T> {
    private final T result;
    private final boolean success;

    private Result(final T result, final boolean success) {
        this.result = result;
        this.success = success;
    }

    public static <T> Result success(final T result) {
        return new Result(result, true);
    }

    public static <T> Result fail(final T result) {
        return new Result(result, false);
    }
}
