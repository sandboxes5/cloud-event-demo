package org.example.misc;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class DeserializationResult {
    private String payloadDeserializer;
    private String cloudEventDeserializer;
    private Result result;
}
