package org.example.visualizer;

import org.example.misc.Result;

public class VerboseSerializeMessageGenerator implements SerializeMessageGenerator {
    @Override
    public void generateMessage(final String cloudEventSerializerDesc, final String payloadSerializerDesc, final Result result) {

        System.out.println("    --------------------------------------------------------------------------------");
        System.out.printf("    Event serialized %s and payload serialized %s: %s%n", cloudEventSerializerDesc, payloadSerializerDesc, resultCalculator(result));
        System.out.println("    " + result.getResult());
        System.out.println("    --------------------------------------------------------------------------------");

//        if(result.isSuccess()) {
//            try {
//                final CloudEvent cloudEvent = ObjectMappers.OM_WITH_CLOUD_EVENT_MODULE.getObjectMapper().readValue(result.getResult().toString(), CloudEvent.class);
//                System.out.println("    " + cloudEvent.getData().getClass().getName());
//            } catch (final Exception ex) {
//                System.out.println("    FAILED to parse with cloud event registered: " + ex.getMessage());
//            }
//            System.out.println("    --------------------------------------------------------------------------------");
//            try {
//                final CloudEvent cloudEvent = ObjectMappers.OM_WITHOUT_CLOUD_EVENT_MODULE.getObjectMapper().readValue(result.getResult().toString(), CloudEvent.class);
//                System.out.println("    " + cloudEvent.getData().getClass().getName());
//            } catch (final Exception ex) {
//                System.out.println("    FAILED to parse without cloud event registered: " + ex.getMessage());
//            }
//            System.out.println("    --------------------------------------------------------------------------------");
//        }
        System.out.println();
    }

    private String resultCalculator(final Result result) {
        return result.isSuccess() ? "SUCCEED" : "FAILED";
    }
}
