package org.example.visualizer;

import org.example.misc.Result;

public class SimpleSerializeMessageGenerator implements SerializeMessageGenerator {
    @Override
    public void generateMessage(String cloudEventSerializerDesc, String payloadSerializerDesc, Result result) {

        System.out.printf("    Event serialized %s and payload serialized %s: %s%n", cloudEventSerializerDesc, payloadSerializerDesc, resultCalculator(result));
    }

    private String resultCalculator(final Result result) {
        return result.isSuccess() ? "SUCCEED" : "FAILED";
    }
}
