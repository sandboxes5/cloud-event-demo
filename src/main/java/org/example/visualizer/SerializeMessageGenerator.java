package org.example.visualizer;

import org.example.misc.Result;

public interface SerializeMessageGenerator {
    void generateMessage(String cloudEventSerializerDesc, String payloadSerializerDesc, Result result);
}
