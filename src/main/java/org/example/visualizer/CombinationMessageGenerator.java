package org.example.visualizer;

import org.example.misc.Result;

import java.util.function.BiPredicate;

public interface CombinationMessageGenerator {
    void generateMessage(
            String cloudEventSerializerDesc,
            String payloadSerializerDesc,
            Result serializationResult,
            String cloudEventDeserializerDesc,
            String payloadDeserializerDesc,
            Result deserializationResult,
            BiPredicate<Result, Result> filter
    );
}
