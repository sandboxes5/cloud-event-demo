package org.example.visualizer;

import org.example.misc.Result;

import java.util.function.BiPredicate;

public class SimpleCombinationMessageGenerator implements CombinationMessageGenerator {
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";

    @Override
    public void generateMessage(
            final String cloudEventSerializerDesc,
            final String payloadSerializerDesc,
            final Result serializationResult,
            final String cloudEventDeserializerDesc,
            final String payloadDeserializerDesc,
            final Result deserializationResult,
            final BiPredicate<Result, Result> filter) {

        if (filter != null && !filter.test(serializationResult, deserializationResult)) return;

        System.out.printf("┌-------------------------------------------------------------------------------------------------------------------------┐%n");
        System.out.printf("|%n");
        System.out.printf("| Event serialized %s and payload serialized %s: %s%n", cloudEventSerializerDesc, payloadSerializerDesc, resultCalculator(serializationResult));
        if (serializationResult.isSuccess()) {
            System.out.printf("| Event deserialized %s and payload deserialized %s: %s%n", cloudEventDeserializerDesc, payloadDeserializerDesc, resultCalculator(deserializationResult));
        }
        System.out.printf("| %n");
        System.out.printf("└-------------------------------------------------------------------------------------------------------------------------┘%n");
        System.out.printf("%n");
    }

    private String resultCalculator(final Result result) {
        return result.isSuccess() ?
                ANSI_GREEN + "SUCCEED" + ANSI_RESET :
                ANSI_RED + "FAILED" + ANSI_RESET;
    }
}
