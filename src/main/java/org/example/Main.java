package org.example;

import lombok.SneakyThrows;
import org.example.converter.deserialize.CloudEventDeserializer;
import org.example.converter.deserialize.PayloadDeserializer;
import org.example.converter.serialize.CloudEventSerializer;
import org.example.converter.serialize.PayloadSerializer;
import org.example.domain.Address;
import org.example.domain.User;
import org.example.misc.Result;
import org.example.misc.SimpleDeserializationCase;
import org.example.misc.SimpleSerializationCase;
import org.example.visualizer.CombinationMessageGenerator;
import org.example.visualizer.SimpleCombinationMessageGenerator;

import java.time.LocalDate;

public class Main {
    @SneakyThrows
    public static void main(String[] args) {
        final Address address1 = new Address();
        address1.setCity("Rome");
        address1.setStreet("Via Urbana");
        final User user1 = new User();
        user1.setName("user1");
        user1.setId(1L);
        user1.setBirthDate(LocalDate.of(1984, 3, 6));
        user1.setAddress(address1);

        final User user2 = new User();
        user2.setName("user2");
        user2.setId(2L);
        user2.setBirthDate(LocalDate.of(1985, 2, 16));

        final User[] users = {
                user1,
                user2
        };

        final CloudEventSerializer[] cloudEventSerializers = {
                CloudEventSerializer.WITH_CLOUD_EVENT_MODULE,
                CloudEventSerializer.WITHOUT_CLOUD_EVENT_MODULE
        };
        final PayloadSerializer[] payloadSerializers = {
                PayloadSerializer.WITH_CLOUD_EVENT_MODULE,
                PayloadSerializer.WITHOUT_CLOUD_EVENT_MODULE,
//                PayloadSerializer.WITH_JAVA_SERIALIZER, // too much binary :S
//                PayloadSerializer.WITH_POJO_CLOUD_EVENT_DATA // too much binary :S
        };

        final CloudEventDeserializer[] cloudEventDeserializers = {
                CloudEventDeserializer.WITHOUT_CLOUD_EVENT_MODULE,
                CloudEventDeserializer.WITH_CLOUD_EVENT_MODULE,
        };


        final PayloadDeserializer[] payloadDeserializers = {
                PayloadDeserializer.TEXT_NODE_WITHOUT_CLOUD_EVENT_MODULE,
                PayloadDeserializer.OBJECT_NODE_WITHOUT_CLOUD_EVENT_MODULE,
                PayloadDeserializer.TO_BYTE_ARRAY_WITHOUT_CLOUD_EVENT_MODULE,
                PayloadDeserializer.TEXT_NODE_WITH_CLOUD_EVENT_MODULE,
                PayloadDeserializer.OBJECT_NODE_WITH_CLOUD_EVENT_MODULE,
                PayloadDeserializer.TO_BYTE_ARRAY_WITH_CLOUD_EVENT_MODULE,
        };

        final CombinationMessageGenerator combinationMessageGenerator = new SimpleCombinationMessageGenerator();

        for (User user : users) {
            System.out.printf("------------------------------- Testing simple serialization cases with user '%s' -------------------------------%n", user.getName());
            System.out.println();

            for (CloudEventSerializer cloudEventSerializer : cloudEventSerializers) {
                for (PayloadSerializer payloadSerializer : payloadSerializers) {
                    final Result<String> serializationResult = new SimpleSerializationCase(cloudEventSerializer, payloadSerializer).execute(user);

                    for (CloudEventDeserializer cloudEventDeserializer : cloudEventDeserializers) {
                        for (PayloadDeserializer payloadDeserializer : payloadDeserializers) {
                            Result deserializationResult = null;
                            if (serializationResult.isSuccess()) {
                                deserializationResult = new SimpleDeserializationCase(payloadDeserializer, cloudEventDeserializer).execute(serializationResult.getResult());
                            }
                            combinationMessageGenerator.generateMessage(
                                    cloudEventSerializer.getDesc(),
                                    payloadSerializer.getDesc(),
                                    serializationResult,
                                    cloudEventDeserializer.getDesc(),
                                    payloadDeserializer.getDesc(),
                                    deserializationResult,
                                    (s, d) -> s != null && s.isSuccess() && d != null && d.isSuccess()
                            );
                        }
                    }
                }
            }
        }
    }
}