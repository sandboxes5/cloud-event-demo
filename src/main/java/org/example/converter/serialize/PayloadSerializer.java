package org.example.converter.serialize;

import io.cloudevents.CloudEventData;
import io.cloudevents.core.data.BytesCloudEventData;
import io.cloudevents.core.data.PojoCloudEventData;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.example.domain.User;
import org.example.misc.ObjectMappers;
import org.example.misc.Result;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;

import static org.example.misc.Result.fail;
import static org.example.misc.Result.success;

@AllArgsConstructor
@Getter
public abstract class PayloadSerializer {
    public static final PayloadSerializer WITH_CLOUD_EVENT_MODULE = new PayloadSerializer("with cloud event module") {
        @Override
        public Result<CloudEventData> serialize(final User user) {
            try {
                String jsonValue = ObjectMappers.OM_WITH_CLOUD_EVENT_MODULE.getObjectMapper().writeValueAsString(user);
                return success(PojoCloudEventData.wrap(jsonValue, String::getBytes));
            } catch (final Exception ex) {
                return fail("FAILED (payload): " + ex.getMessage());
            }
        }
    };

    public static final PayloadSerializer WITHOUT_CLOUD_EVENT_MODULE = new PayloadSerializer("without cloud event module") {
        @Override
        public Result<CloudEventData> serialize(final User user) {
            try {
                String jsonValue = ObjectMappers.OM_WITHOUT_CLOUD_EVENT_MODULE.getObjectMapper().writeValueAsString(user);
                return success(PojoCloudEventData.wrap(jsonValue, String::getBytes));
            } catch (final Exception ex) {
                return fail("FAILED (payload): " + ex.getMessage());
            }
        }
    };

    public static final PayloadSerializer WITH_JAVA_SERIALIZER = new PayloadSerializer("with Java serializer into BytesCloudEventData") {
        @Override
        public Result<CloudEventData> serialize(final User user) {
            try (final ByteArrayOutputStream bos = new ByteArrayOutputStream();
                 final ObjectOutputStream oos = new ObjectOutputStream(bos)) {
                oos.writeObject(user);
                oos.flush();

                return success(BytesCloudEventData.wrap(bos.toByteArray()));
            } catch (final Exception ex) {
                return fail("FAILED (payload): " + ex.getMessage());
            }
        }
    };

    public static final PayloadSerializer WITH_POJO_CLOUD_EVENT_DATA = new PayloadSerializer("with Java serializer into PojoCloudEventData") {
        @Override
        public Result<CloudEventData> serialize(final User user) {
            try {
                return success(PojoCloudEventData.wrap(user, userP -> {
                    try (final ByteArrayOutputStream bos = new ByteArrayOutputStream();
                         final ObjectOutputStream oos = new ObjectOutputStream(bos)) {
                        oos.writeObject(userP);
                        oos.flush();

                        return bos.toByteArray();
                    }
                }));
            } catch (final Exception ex) {
                return fail("FAILED (payload): " + ex.getMessage());
            }
        }
    };

    private final String desc;

    public abstract Result<CloudEventData> serialize(final User user);
}
