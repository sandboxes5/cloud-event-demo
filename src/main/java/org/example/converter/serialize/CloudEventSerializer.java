package org.example.converter.serialize;

import io.cloudevents.CloudEvent;
import io.cloudevents.CloudEventData;
import io.cloudevents.core.v1.CloudEventBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.example.domain.User;
import org.example.misc.ObjectMappers;
import org.example.misc.Result;

import java.net.URI;

import static org.example.misc.Result.fail;
import static org.example.misc.Result.success;

@AllArgsConstructor
@Getter
public abstract class CloudEventSerializer {
    public static final CloudEventSerializer WITHOUT_CLOUD_EVENT_MODULE = new CloudEventSerializer("without cloud event module") {
        @Override
        public Result<String> serialize(User user, CloudEventData cloudEventData) {
            CloudEvent cloudEvent = buildCloudEvent(user, cloudEventData);
            try {
                return success(ObjectMappers.OM_WITHOUT_CLOUD_EVENT_MODULE.getObjectMapper().writeValueAsString(cloudEvent));
            } catch (final Exception ex) {
                return fail("FAILED: " + ex.getMessage());
            }
        }
    };
    public static final CloudEventSerializer WITH_CLOUD_EVENT_MODULE = new CloudEventSerializer("with cloud event module") {
        @Override
        public Result<String> serialize(User user, CloudEventData cloudEventData) {
            CloudEvent cloudEvent = buildCloudEvent(user, cloudEventData);
            try {
                return success(ObjectMappers.OM_WITH_CLOUD_EVENT_MODULE.getObjectMapper().writeValueAsString(cloudEvent));
            } catch (final Exception ex) {
                return fail("FAILED: " + ex.getMessage());
            }
        }
    };

    private final String desc;

    public abstract Result<String> serialize(final User user, final CloudEventData cloudEventData);

    protected CloudEvent buildCloudEvent(final User user, final CloudEventData cloudEventData) {
        return new CloudEventBuilder().
                withId(Long.toString(user.getId())).
                withSubject(user.getName()).
                withType("entity.user").
                withSource(URI.create("example.local")).
                withData(cloudEventData).
                build();
    }
}
