package org.example.converter.deserialize;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import io.cloudevents.CloudEventData;
import io.cloudevents.jackson.JsonCloudEventData;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.example.domain.User;
import org.example.misc.Result;

import static org.example.misc.ObjectMappers.OM_WITHOUT_CLOUD_EVENT_MODULE;
import static org.example.misc.ObjectMappers.OM_WITH_CLOUD_EVENT_MODULE;
import static org.example.misc.Result.fail;
import static org.example.misc.Result.success;

@AllArgsConstructor
@Getter
public abstract class PayloadDeserializer {
    public static final PayloadDeserializer TEXT_NODE_WITHOUT_CLOUD_EVENT_MODULE = new PayloadDeserializer("text node without cloud event module") {

        @Override
        public Result deserialize(CloudEventData cloudEventData) {
            if (cloudEventData instanceof JsonCloudEventData) {
                final JsonCloudEventData jsonCloudEventData = (JsonCloudEventData) cloudEventData;
                if (jsonCloudEventData.getNode() instanceof TextNode) {
                    final TextNode textNode = (TextNode) jsonCloudEventData.getNode();
                    try {
                        return success(OM_WITHOUT_CLOUD_EVENT_MODULE.getObjectMapper().readValue(textNode.asText(), User.class));
                    } catch (final Exception ex) {
                        return fail(ex.getMessage());
                    }
                }
                return fail("JsonCloudEventData's node is not TextNode");
            }
            return fail("CloudEventData is not JsonCloudEventData");
        }
    };

    public static final PayloadDeserializer OBJECT_NODE_WITHOUT_CLOUD_EVENT_MODULE = new PayloadDeserializer("object node without cloud event module") {

        @Override
        public Result deserialize(CloudEventData cloudEventData) {
            if (cloudEventData instanceof JsonCloudEventData) {
                final JsonCloudEventData jsonCloudEventData = (JsonCloudEventData) cloudEventData;
                if (jsonCloudEventData.getNode() instanceof ObjectNode) {
                    final ObjectNode objectNode = (ObjectNode) jsonCloudEventData.getNode();
                    try {
                        return success(OM_WITHOUT_CLOUD_EVENT_MODULE.getObjectMapper().readValue(objectNode.toString(), User.class));
                    } catch (final Exception ex) {
                        return fail(ex.getMessage());
                    }
                }
                return fail("JsonCloudEventData's node is not ObjectNode");
            }
            return fail("CloudEventData is not JsonCloudEventData");
        }
    };

    public static final PayloadDeserializer TO_BYTE_ARRAY_WITHOUT_CLOUD_EVENT_MODULE = new PayloadDeserializer("with toBytes without cloud event module") {
        @Override
        public Result deserialize(CloudEventData cloudEventData) {
            try {
                return success(OM_WITHOUT_CLOUD_EVENT_MODULE.getObjectMapper().readValue(cloudEventData.toBytes(), User.class));
            } catch (final Exception ex) {
                return fail(ex.getMessage());
            }
        }
    };

    public static final PayloadDeserializer TEXT_NODE_WITH_CLOUD_EVENT_MODULE = new PayloadDeserializer("text node with cloud event module") {

        @Override
        public Result deserialize(CloudEventData cloudEventData) {
            if (cloudEventData instanceof JsonCloudEventData) {
                final JsonCloudEventData jsonCloudEventData = (JsonCloudEventData) cloudEventData;
                if (jsonCloudEventData.getNode() instanceof TextNode) {
                    final TextNode textNode = (TextNode) jsonCloudEventData.getNode();
                    try {
                        return success(OM_WITH_CLOUD_EVENT_MODULE.getObjectMapper().readValue(textNode.asText(), User.class));
                    } catch (final Exception ex) {
                        return fail(ex.getMessage());
                    }
                }
                return fail("JsonCloudEventData's node is not TextNode");
            }
            return fail("CloudEventData is not JsonCloudEventData");
        }
    };

    public static final PayloadDeserializer OBJECT_NODE_WITH_CLOUD_EVENT_MODULE = new PayloadDeserializer("object node with cloud event module") {

        @Override
        public Result deserialize(CloudEventData cloudEventData) {
            if (cloudEventData instanceof JsonCloudEventData) {
                final JsonCloudEventData jsonCloudEventData = (JsonCloudEventData) cloudEventData;
                if (jsonCloudEventData.getNode() instanceof ObjectNode) {
                    final ObjectNode objectNode = (ObjectNode) jsonCloudEventData.getNode();
                    try {
                        return success(OM_WITH_CLOUD_EVENT_MODULE.getObjectMapper().readValue(objectNode.toString(), User.class));
                    } catch (final Exception ex) {
                        return fail(ex.getMessage());
                    }
                }
                return fail("JsonCloudEventData's node is not ObjectNode");
            }
            return fail("CloudEventData is not JsonCloudEventData");
        }
    };

    public static final PayloadDeserializer TO_BYTE_ARRAY_WITH_CLOUD_EVENT_MODULE = new PayloadDeserializer("with toBytes with cloud event module") {
        @Override
        public Result deserialize(CloudEventData cloudEventData) {
            try {
                return success(OM_WITH_CLOUD_EVENT_MODULE.getObjectMapper().readValue(cloudEventData.toBytes(), User.class));
            } catch (final Exception ex) {
                return fail(ex.getMessage());
            }
        }
    };
    private final String desc;

    public abstract Result deserialize(final CloudEventData cloudEventData);
}
