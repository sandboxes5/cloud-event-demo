package org.example.converter.deserialize;

import io.cloudevents.CloudEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.example.misc.ObjectMappers;
import org.example.misc.Result;

import static org.example.misc.Result.fail;
import static org.example.misc.Result.success;

@AllArgsConstructor
@Getter
public abstract class CloudEventDeserializer {
    public static final CloudEventDeserializer WITHOUT_CLOUD_EVENT_MODULE = new CloudEventDeserializer("without cloud event module") {
        @Override
        public Result<CloudEvent> deserialize(final String json) {
            try {
                return success(ObjectMappers.OM_WITHOUT_CLOUD_EVENT_MODULE.getObjectMapper().readValue(json, CloudEvent.class));
            } catch (final Exception ex) {
                return fail(ex.getMessage());
            }
        }
    };
    public static final CloudEventDeserializer WITH_CLOUD_EVENT_MODULE = new CloudEventDeserializer("with cloud event module") {
        @Override
        public Result<CloudEvent> deserialize(final String json) {
            try {
                return success(ObjectMappers.OM_WITH_CLOUD_EVENT_MODULE.getObjectMapper().readValue(json, CloudEvent.class));
            } catch (final Exception ex) {
                return fail(ex.getMessage());
            }
        }
    };
    private final String desc;

    public abstract Result<CloudEvent> deserialize(final String json);
}
